# AntMusic - A simple music player

## Compile from source

Clone the repo into a local folder, then run:

```bash
# Download and install dependencies
npm install

# Run the server
npm run dev

# Build app
npm run build
```